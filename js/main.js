window.addEventListener("impress:stepenter", function (event) {
    console.log("HASH CHANGE", location.hash);
    switch (location.hash) {
        //case "#/BP_LIST": $(event.target.children[0]).animo({animation: 'bounceIn', duration: 1}); break;
        case ('#/GETSTART') : $('#GETSTART img:nth-child(1)').animo({animation: 'flipInY', duration: 2}); break;
    }
});
document.addEventListener('impress:substepactive', function(event){
    var curSubStep = event.target.id;
    var curSubParent = event.target.parentNode.id;
    console.log('CURRENT STEP PARENT', curSubParent);
    console.log('CURRENT STEP', curSubStep);
    /*Создание анимаций при от объекта события*/
    switch (curSubStep) {
        case ('combo-fluid-fixed') : $('#combo-fluid-fixed :nth-child(1)').animo({animation: 'slideInLeft', duration: 2});
            console.log('combo-fluid-fixed 112'); break;
    }
    /*Создание анимаций при от родителя объекта события*/
    switch (curSubParent) {
        case ('PERSONALUSER') : $(event.target).animo({animation: 'tada', duration: 1}); break;
        case ('CTI') : $(event.target).animo({animation: 'tada', duration: 1}); break;
        case ('SOCIALNETWORKS') : $(event.target).animo({animation: 'tada', duration: 1}); break;
        case ('WORK') : $(event.target).animo({animation: 'bounceIn', duration: 1}); break;
        case ('EMPLOYEE_CONTROL') : $(event.target).animo({animation: 'bounceIn', duration: 1}); break;
        case ('FINANCE_BENEFITS') : $(event.target).animo({animation: 'tada', duration: 1}); break;
        case ('QM') : $(event.target).animo({animation: 'bounceIn', duration: 1}); break;
    }
});
$('.flipcard').on('click', function() {
    $(this).toggleClass('flip');
});


